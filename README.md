# Application Blueprint for Minecraft

## What is Minecraft?
Minecraft is an optionally multiplayer sandbox video game developed by Mojang Studios.

**Official Website:** [minecraft.net](https://www.minecraft.net/)

This application blueprint uses the [itzg/minecraft-server](https://hub.docker.com/r/itzg/minecraft-server) Docker image.

**By deploying this application blueprint, you agree to the [terms of Minecraft's EULA](https://www.minecraft.net/eula).**

## Filling out a deployment blueprint
For the general steps to deploy an application with Unfurl Cloud, see our [Getting Started guides](https://unfurl.cloud/help#guides).

### Components
Each application blueprint includes **components**. These are the required and optional resources for the application. In most cases, there is more than one way to fulfill a component requirement. After you select a deployment blueprint, you will be prompted to fulfill the component requirements and configure the deployment to your needs. Common components include Compute, DNS, Database, and Mail Server:

#### Compute

Minecraft requires the following compute resources:

- At least 1 CPU
- At least 3072MB of RAM (3GB)
- At least 16GB of hard disk space
